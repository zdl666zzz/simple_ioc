## 介绍
如名 简单上手 易于操作的注入工具 

## 使用
#### 链式注入
```ts
// file handle.ts

export class MathHandle {
  public intMath: number = 10;

  public add() {
    this.intMath++;
  }

  constructor() {}
}

// file script.ts

@Service() // =====> 装饰 @Service 的类被标记为可初始化的容器
export class Script {
  @Inject()
  mathHandle?: MathHandle;

  public demo: number = 1;

  constructor() {}
}


// file index.ts
@Root() // =====> 初始化全部被标记的容器
@Service()
export class Index {
  @Inject()
  script?: Script;


  constructor() {}
}

let index = new Index()
console.log(index.script?.demo) // =====> 1
console.log(index.script?.mathHandle?.intMath) // =====> 10

```
#### 单例模式 

```ts
// file script.ts

@Service({key:"script"})
export class Script {
  @Inject()
  mathHandle?: MathHandle;

  constructor() {}
}

@Root()
@Service({key:"script"})
export class Script2 {
  @Inject()
  mathHandle?: MathHandle;

  constructor() {}
}

let script = new Script()
let script2 = new Script2()
script.mathHandle?.add()
console.log(script2.mathHandle?.intMath) // =====> 11

```
#### 后执行与销毁

```ts
// file script.ts
@Service()
export class Script {
  @Inject()
  mathHandle?: MathHandle;

  constructor() {
    this.getMath() //需要直接在构造器中调用注入时 请将逻辑放在函数中 之后给方法装饰 @Already
  }
  
  @Already()
  getMath(){
    console.log(this.mathHandle?.intMath)
  }

  @Destroy() //需要清除此容器中的注入与单例注入时，请在方法上装饰 @Destroy 当调用该方法时执行销毁操作
  delIoc(){
    console.log(this.mathHandle?.intMath)
  }

}

let script = new Script()
script.delIoc() // =====> 10
console.log(script.mathHandle?.intMath) // =====> undefined

```
#### 循环注入

```ts

// file handle.ts

@Service()
export class MathHandle {

  @InjectRef(()=>Script) // =====> 使用InjectRef进行循环注入
  script?: Script;

  constructor() {}
}

// file script.ts

@Service()
export class Script {

  @InjectRef(()=>MathHandle)
  mathHandle?: MathHandle;

  constructor() {}
}


```
