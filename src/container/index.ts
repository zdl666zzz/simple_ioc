import { Token } from "../utils";

export default class Container {
  private static containerMap = new Map<string, Container>();

  private dataMap = new Map<Token, Object>();

  static factory(key: string): Container | undefined;
  static factory(key: string, create: true): Container;
  static factory(key: string, create: boolean = false) {
    if (!this.containerMap.has(key)) {
      if (create) {
        this.containerMap.set(key, new this(key));
      }
    }
    return this.containerMap.get(key);
  }

  constructor(private key: string) {}

  getData(token: Token) {
    return this.dataMap.get(token);
  }

  setData(token: Token, instance: Object) {
    if (!this.dataMap.has(token)) {
      this.dataMap.set(token, instance);
    }
  }

  destroy() {
    this.dataMap.clear();
    Container.containerMap.delete(this.key);
  }
}
