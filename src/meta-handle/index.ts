import Injection from "../injection";

//在容器的原型对象上绑定inject、销毁方法、容器初始化后立即执行的方法（元数据形式）
export default class MetaHandle {
  private static key = {
    injection: Symbol("injectionKey"),
  };

  static setInjection(
    target: Constructor,
    options: ConstructorParameters<typeof Injection>[0]
  ): void {
    
    Reflect.defineMetadata(
      this.key.injection,
      this.getInjection(target).add(new Injection(options)),
      target
    );
  }

  static getInjection(target: Constructor): Set<Injection> {
    return Reflect.getMetadata(this.key.injection, target) || new Set();
  }

  static deleteInjection(target: Constructor) {
    Reflect.deleteMetadata(this.key.injection, target);
  }

 
}
