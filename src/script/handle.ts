import { Already, Destroy, Inject, Root, Service } from "..";

export class MathHandle {
  public intMath: number = 10;

  public add() {
    this.intMath++;
  }

  constructor() {}
}

@Service()
export class Script1 {
  @Inject()
  mathHandle?: MathHandle;

  public demo: number = 1;

  constructor() {}
}
