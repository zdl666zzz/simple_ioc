import { Already, Destroy, Inject, Root, Service } from "..";
import {Script1} from "./handle";

@Root()
@Service()
class Script2 {
  @Inject()
  script1?: Script1;

  constructor() {
    this.getData()
  }
  
  @Already()
  getData(){
    console.log(this.script1?.demo)
  } 

  @Destroy()
  killIoc(){
    console.log(this.script1?.demo)
  }

}


let a =  new Script2()
a.killIoc()
console.log(a.script1?.mathHandle?.intMath)

let b = new Script1()
console.log(b.mathHandle?.intMath)