
export function isObject(val:unknown):val is Object{
    return val instanceof Object
}


export function isConstructor(val:unknown):val is Constructor{
    return typeof val === 'function' && val !== Object
}