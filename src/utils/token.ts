export default  class Token {
  static tokenMap = new Map<Constructor, Token>();

  static create(target: Constructor) {
    if (this.tokenMap.has(target)) {
      return this.tokenMap.get(target);
    }
    let token = new this(target);

    this.tokenMap.set(target, token);
    return token;
  }

  constructor( public target: Constructor) {}
}
